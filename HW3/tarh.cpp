#include <iostream>
#include <list>
#include <limits>
#include <vector>

using namespace std;

int V;
int* dist;
list<int>* adj;
bool* hospital;
vector<int> hospitalV;

void BFSk(int s) {
    bool *visited = new bool[V];
    int* innerDist = new int[V];

    for(int i = 0; i < V; i++) {
        visited[i] = false;
        innerDist[i] = std::numeric_limits<int>::max();
    }
    for(int& h : hospitalV) {
        visited[h] = true;
    }

    list<int> queue;

    visited[s] = true;
    innerDist[s] = 0;
    queue.push_back(s);

    while(!queue.empty()) {
        s = queue.front();
        queue.pop_front();
        for(list<int>::iterator i = adj[s].begin(); i != adj[s].end(); ++i) {
            if (!visited[*i]) {
                visited[*i] = true;
                innerDist[*i] = innerDist[s] + 1;
                dist[*i] = min(dist[*i], innerDist[*i]);
                queue.push_back(*i);
            }
        }
    }
    delete[] visited;
    delete[] innerDist;
}

void BFS(int start) {
    bool* visited = new bool[V];
    int* innerDist = new int[V];

    for(int i = 0; i < V; i++) {
        visited[i] = false;
        innerDist[i] = std::numeric_limits<int>::max();
    }

    list<int> queue;

    visited[start] = true;
    innerDist[start] = 0;
    queue.push_back(start);

    while(!queue.empty()) {
        int s = queue.front();
        queue.pop_front();
        for(list<int>::iterator i = adj[s].begin(); i != adj[s].end(); ++i) {
            if (!visited[*i]) {
                visited[*i] = true;
                innerDist[*i] = innerDist[s] + 1;

                if(hospital[*i]) {
                    dist[start] = innerDist[*i];
                    delete[] visited;
                    delete[] innerDist;
                    return;
                }

                queue.push_back(*i);
            }
        }
    }

    delete[] visited;
    delete[] innerDist;
}

int main() {
    int E, k;
    cin >> V >> E >> k;

    adj = new list<int>[V];
    dist = new int[V];
    hospital = new bool[V];
    for(int i = 0; i < V; i++) {
        hospital[i] = false;
    }
    for(int i = 0; i < V; i++) {
        dist[i] = std::numeric_limits<int>::max();
    }

    for(int i = 0; i < k; i++) {
        int v;
        cin >> v;
        hospital[v-1] = true;
        hospitalV.push_back(v-1);
        dist[v-1] = 0;
    }
    for(int i = 0; i < E; i++) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }

    int t = V/2 - V/4;
    if(k < t) {
        for(int& s : hospitalV) {
            BFSk(s);
        }
    } else {
        for(int s = 0; s < V; s++) {
            if(!hospital[s])
                BFS(s);
        }
    }

    for(int i = 0; i < V; i++) {
        cout << dist[i] << endl;
    }

    return 0;
}
