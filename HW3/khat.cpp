#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;
typedef long long int LLI;

int main() {
    /* Variables */
    int n;
    cin >> n;
    auto * k = new int[n];
    auto ** p = new int*[n];
    auto ** w = new int*[n];
    auto ** PtoW = new pair<double, int>* [n]; //num, counter
    auto * classP = new LLI[n];
    auto * classW = new LLI[n];
    auto * classPtoW = new pair<double, int> [n]; //num, counter
    int sumOfKi = 0;
    for(int i = 0; i < n; i++) {
        cin >> k[i];
        sumOfKi += k[i];
        p[i] = new int[k[i]];
        w[i] = new int[k[i]];
        PtoW[i] = new pair<double, int>[k[i]];
    }
    auto * wOrder = new int[sumOfKi];
    auto * pOrder = new int[sumOfKi];
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < k[i]; j++) {
            cin >> p[i][j];
        }
    }

    /* PtoW */
    int counter = 0;
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < k[i]; j++) {
            cin >> w[i][j];
            PtoW[i][j].first = static_cast<double>(p[i][j])/static_cast<double>(w[i][j]);
            PtoW[i][j].second = counter;
            pOrder[counter] = p[i][j];
            wOrder[counter] = w[i][j];
            counter++;
        }
    }

    /* PtoW Class */
    counter = 0;
    for(int i = 0; i < n; i++) {
        classP[i] = 0;
        classW[i] = 0;
        for(int j = 0; j < k[i]; j++) {
            classP[i] += p[i][j];
            classW[i] += w[i][j];
        }
        classPtoW[i].first = static_cast<double>(classP[i])/static_cast<double>(classW[i]);
        classPtoW[i].second = counter++;
    }

    /* Cost */
    sort(classPtoW, classPtoW+n);
    LLI time = 0;
    LLI totalCost = 0;
    for(int i = 0; i < n; i++) {
        int classNum = classPtoW[i].second;
        sort(PtoW[classNum], PtoW[classNum]+k[classNum]);
        for(int j = 0; j < k[classNum]; j++) {
            int homeWorkNum = PtoW[classNum][j].second;
            time += pOrder[homeWorkNum];
            totalCost += time * wOrder[homeWorkNum];
        }
    }
    cout << totalCost << endl;

    /* Print HomeWorks */
    for(int i = 0; i < n; i++) {
        int classNum = classPtoW[i].second;
        for(int j = 0; j < k[classNum]; j++) {
            cout << PtoW[classNum][j].second + 1<< " ";
        }
    }

    return 0;
}