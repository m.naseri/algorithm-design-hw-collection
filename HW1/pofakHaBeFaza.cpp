#include <iostream>

typedef unsigned long long int ULL;
typedef long long int LL;

ULL mergeInversion(LL* a, int l, int m, int r) {
    LL* copyArr = new LL[r - l + 1];
    ULL counter = 0;
    int j = m + 1, i = l, k = 0;
    while(i <= m && j <= r) {
        if(a[j] > a[i]) {
            copyArr[k++] = a[i++];
            counter += j - 1 -m;
        }
        else {
            copyArr[k++] = a[j++];
        }
    }

    while(i <= m) {
        copyArr[k++] = a[i++];
        counter += r - m;
    }

    while(j <= r) {
        copyArr[k++] = a[j++];
    }

    for(int o = 0; o < k; o++) {
        a[l + o] = copyArr[o];
    }

    return counter;
}

ULL inversionSort(LL* a, int l, int r) {
    if(r <= l) {
        return 0;
    }
    int m = (l + r)/2;
    ULL c1 = inversionSort(a, l, m);
    ULL c2 = inversionSort(a, m+1, r);
    ULL c3 = mergeInversion(a, l, m, r);

    return c1 + c2 + c3;
}

int main() {
    int n;
    std::cin >> n;

    auto * a = new LL[n];
    for(int i = 0; i < n; i++) {
        std::cin >> a[i];
    }

    ULL counter = inversionSort(a, 0, n - 1);
//    for(int i = 0; i < n; i++) {
//        std::cout << a[i] << " ";
//    }
//    std::cout << std::endl;

    std::cout << counter%1000000007 << std::endl;
    return 0;
}