#include <iostream>
#include <limits>

typedef long long int LLI;

int main() {

    LLI M, S;
    std::cin >> M >> S;
    unsigned long long loopCounter = 0;
    bool possible = false;
    if(M == 1) {
        std::cout << S - 1 << std::endl;
        return 0;
    }
    else if(S == 1) {
        std::cout << M - 1 << std::endl;
        return 0;
    }
    
    while(true) {
        if(M >= S) {
            LLI resultOfDivision = M / S;
            M = M - resultOfDivision*S;
            loopCounter += resultOfDivision;
            if(M == 1) {
                loopCounter += S - 1;
                possible = true;
                break;
            }
            else if(M == 0) {
                break;
            }

        } else if(M < S) {
            LLI resultOfDivision = S / M;
            S = S - resultOfDivision*M;
            loopCounter += resultOfDivision;
            if(S == 1) {
                loopCounter += M - 1;
                possible = true;
                break;
            }
            else if(S == 0) {
                break;
            }
        }
    }

    if(possible) {
        std::cout << loopCounter << std::endl;
    } else {
        std::cout << "impossible" << std::endl;
    }
    return 0;
}