#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <fstream>

using namespace std;

class Graph {
public:
    int V;
    list<int> *adj;

    explicit Graph(int V) {
        this->V = V;
        adj = new list<int>[V];
    };

    void BFS(int s, int* parents) {
        auto *visited = new bool[V];
        for(int i = 0; i < V; i++) {
            visited[i] = false;
            parents[i] = -1;
        }

        list<int> queue;

        visited[s] = true;
        parents[s] = s;
        queue.push_back(s);

        while(!queue.empty()) {
            s = queue.front();
            queue.pop_front();

            for(int &i : adj[s]) {
                if (!visited[i]) {
                    visited[i] = true;
                    parents[i] = s;
                    queue.push_back(i);
                }
            }
        }
    }

    void addEdge(int v, int w) {
        adj[v].push_back(w);
    }

};

bool isConnected(string str1, string str2) {
    transform(str1.begin(), str1.end(), str1.begin(), ::tolower);
    transform(str2.begin(), str2.end(), str2.begin(), ::tolower);
    if(str1.size() == str2.size()) {
        int numOfConflicts = 0;
        for(int i = 0; i < str1.size(); i++) {
            if(str1[i] != str2[i]) {
                numOfConflicts++;
                if(numOfConflicts > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    string* strL;
    string* strS;
    if(str1.size() > str2.size()) {
        strL = &str1;
        strS = &str2;
    } else {
        strL = &str2;
        strS = &str1;
    }

    if(strL->size() - strS->size() > 1)
        return false;

    int numOfConflicts = 0, i = 0, j = 0;
    while(i < strL->size()) {
        if((*strL)[i] == (*strS)[j]) {
            i++;
            j++;
        } else {
            i++;
            numOfConflicts++;
            if(numOfConflicts > 1) {
                return false;
            }
        }
    }
    return true;
}


class node {
public:
    node(string str, int num) {
        this->str = std::move(str);
        this->num = num;
    }
    string str;
    int num;
};

bool myCompare(node a, node b) {
    return a.str < b.str;
}

int main() {
//    ofstream cout;
//    cout.open("out.txt");
//
//    ifstream cin("in.txt");
//    if(!cin.is_open()) {
//        return 0;
//    }

    int k, q;
    cin >> k >> q;
    vector<node> nodes;
    vector<node> originalNodes;

    /* cin Dictionary */
    for(int i = 0; i < k; i++) {
        string temp;
        cin >> temp;
        originalNodes.emplace_back(temp, i);
//        transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
        nodes.emplace_back(temp, i);
    }

    /* cin Queries */
    vector<pair<string, string>> queries;
    for(int i = 0; i < q; i++) {
        string str1, str2;
        cin >> str1 >> str2;
//        transform(str1.begin(), str1.end(), str1.begin(), ::tolower);
//        transform(str2.begin(), str2.end(), str2.begin(), ::tolower);
        queries.emplace_back(str1, str2);
    }

    /* Order & Sort */
    vector<node> orderedNodes;
    for(auto& elem : nodes) {
        orderedNodes.push_back(elem);
    }
    sort(nodes.begin(), nodes.end(), myCompare);

    /* Graph & Edge */
    Graph G(k);
    for(int i = 0; i < k; i++) {
        for(int j = i+1; j < k; j++) {
            if(isConnected(orderedNodes[i].str, orderedNodes[j].str)) {
                G.addEdge(i, j);
                G.addEdge(j, i);
            }
        }
    }

    /* BFS */
    auto ** BFSParents = new int*[k];
    for(int i = 0; i < k; i++) {
        BFSParents[i] = new int[k];
    }
    for(int i = 0; i < k; i++) {
        G.BFS(i, BFSParents[i]);
    }

    /* Queries */
    for(int i = 0; i < queries.size(); i++) {
        auto lower1 = std::lower_bound(nodes.begin(), nodes.end(), node(queries[i].first, 0), myCompare);
        auto lower2 = std::lower_bound(nodes.begin(), nodes.end(), node(queries[i].second, 0), myCompare);
        if((lower1->str != queries[i].first) || (lower2->str != queries[i].second)) {
            cout << "*";
//            if(i != queries.size()-1)
            cout << "\n";
            continue;
        }

        int n1 = lower1->num;
        int n2 = lower2->num;
        if(lower1->str == lower2->str) {
            cout << originalNodes[n1].str << " " << originalNodes[n2].str;
//            if(i != queries.size()-1)
            cout << "\n";
            continue;
        }

        if(BFSParents[n1][n2] == -1) {
            cout << "*";
//            if(i != queries.size()-1)
            cout << "\n";
            continue;
        }

        vector<int> path;
        int nodeIndex = n2;
        path.push_back(n2);
        while(true) {
            nodeIndex = BFSParents[n1][nodeIndex];
            path.push_back(nodeIndex);
            if(nodeIndex == n1) {
                break;
            }
        }

        for(long j = path.size() - 1; j >= 0; j--) {
            cout << originalNodes[path[j]].str << " ";
        }
//        if(i != queries.size()-1)
        cout << "\n";
    }

//    cout.close();
//    cin.close();
    return 0;
}
