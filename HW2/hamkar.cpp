#include <iostream>

using namespace std;

int* t;
int** cache;

int maxNumOfPapers(int EBusy, int n) {
    if(n < 0) {
        return 0;
    }
    if(cache[n][EBusy] != -1) {
        return cache[n][EBusy];
    }

    int sum = 0, m = n - 1;
    while(EBusy + sum < t[n]) {
        if(m < 0) {
            break;
        }
        sum += t[m--];
    }

    return cache[n][EBusy] = max(1 + maxNumOfPapers(sum + EBusy - t[n], m), maxNumOfPapers(t[n], n - 1));

}


int main() {
    int n;
    cin >> n;
    t = new int[n];
    int J = -1;
    for(int i = 0; i < n; i++) {
        cin >> t[i];
        J = max(t[i], J);
    }
    J++;

//    int n = 7;
//    int J = 42;
//    t = new int[n]{29, 16, 5, 7, 27, 10, 41};

//    int n = 4;
//    int J = 91;
//    t = new int[n]{80, 50, 10, 90};

    cache = new int*[n];
    for(int i = 0; i < n; i++) {
        cache[i] = new int[J];
    }

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < J; j++) {
            cache[i][j] = -1;
        }
    }
//    for(int j = 0; j < J; j++) {
//        cache[0][j] = 0;
//    }
//    for(int i = 1; i < 3; i++) {
//        for(int j = 0; j < J; j++) {
//            cache[i][j] = 1;
//        }
//    }

    cout << maxNumOfPapers(0, n - 1) << endl;

    for(int i = 0; i < n; i++) {
        delete[] cache[i];
    }
    delete[] cache;
    delete[] t;
    return 0;
}