#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef long long int LL;

LL TASortNumber(vector<LL>& A, vector<vector<LL>>& cache, int l, int r) {
    // Sorts [l, r)
    if(cache[l][r] != -1)
        return cache[l][r];

    if(l > r)
        return 1;

    if(is_sorted(A.begin() + l, A.begin() + r))
        return 1;

    int mid = (l + r) / 2;
    return cache[l][r] = 1 + TASortNumber(A, cache, l, mid) + TASortNumber(A, cache, mid, r);
}


int main() {
    unsigned long n, q;
    cin >> n >> q;
    vector<LL> A(n);
    vector<vector<LL>> cache(n);

    for(auto& elem : cache) {
        elem = vector<LL>(n + 1);
    }

    for(int i = 0; i < n; ++i) {
        for(int j = 0; j <= n; ++j) {
            cache[i][j] = -1;
        }
    }

    for(int i = 0; i < n; ++i) {
        cin >> A[i];
        cache[i][i] = 1;
    }

    for(int i = 0; i < q; i++) {
        int l, r;
        cin >> l >> r;
        l--;
        r--;
        cout << TASortNumber(A, cache, l, r) << endl;
    }

    return 0;
}