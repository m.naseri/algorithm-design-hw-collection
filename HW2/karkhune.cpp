#include <iostream>
#include <stack>

using namespace std;
typedef long long int LLI;

class stackElem{
public:
    stackElem(char ch, LLI index) {
        this->ch = ch;
        this->index = index;
    }
    char ch;
    LLI index;
};

int main() {
    LLI n;
    cin >> n;
    LLI* tillWhere = new LLI[n];
    LLI* ans = new LLI[n];
    char* CHs = new char[n];

    for(LLI i = 0; i < n; ++i) {
        cin >> CHs[i];
        tillWhere[i] = -1;
    }
    stack<stackElem> s;

    for(LLI i = 0; i < n; ++i) {
        if(CHs[i] == '(' or CHs[i] == '{' or CHs[i] == '<' or CHs[i] == '[') {
            s.push(stackElem{CHs[i], i});
        } else {
            switch(CHs[i]) {
                case ')':
                    if(!s.empty()) {
                        if(s.top().ch == '(') {
                            tillWhere[s.top().index] = i;
                            s.pop();
                        } else {
                            s.push(stackElem{CHs[i], i});
                        }
                    } else {
                        s.push(stackElem{CHs[i], i});
                    }
                    break;

                case '}':
                    if(!s.empty()) {
                        if(s.top().ch == '{') {
                            tillWhere[s.top().index] = i;
                            s.pop();
                        } else {
                            s.push(stackElem{CHs[i], i});
                        }
                    } else {
                        s.push(stackElem{CHs[i], i});
                    }
                    break;

                case ']':
                    if(!s.empty()) {
                        if(s.top().ch == '[') {
                            tillWhere[s.top().index] = i;
                            s.pop();
                        } else {
                            s.push(stackElem{CHs[i], i});
                        }
                    } else {
                        s.push(stackElem{CHs[i], i});
                    }
                    break;

                case '>':
                    if(!s.empty()) {
                        if(s.top().ch == '<') {
                            tillWhere[s.top().index] = i;
                            s.pop();
                        } else {
                            s.push(stackElem{CHs[i], i});
                        }
                    } else {
                        s.push(stackElem{CHs[i], i});
                    }
                    break;
                default:
                    break;
            }
        }
    }

    for(LLI i = n - 1; i > -1; i--) {
        if(tillWhere[i] == -1) {
            ans[i] = 0;
        } else {
            ans[i] = tillWhere[i] - i + 1 + ans[tillWhere[i] + 1];
        }
    }

    for(LLI j = 0; j < n; ++j) {
        cout << ans[j] << " ";
    }

    return 0;
}
